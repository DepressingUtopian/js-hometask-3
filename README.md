# hometask-10

## Ответьте на вопросы

#### 1. Как можно сменить контекст вызова? Перечислите все способы и их отличия друг от друга.
> Ответ: Функции bind/call/apply, переопреределение контекста через присваивание функции.
Функция bind принимеет контекст и параметры и возвращает, возвращает переданную функцию с измененным контекстом.
Функция call работает аналогично apply, отличие в том что функция выполняется сразу после вызова.
Функция apply похожа на call, но параметры передаются в виде массива.

#### 2. Что такое стрелочная функция?
> Ответ: Краткий вид представления функции.
#### 3. Приведите свой пример конструктора. 
```js
// Ответ:
    function init() {
      this.name = 'Alex';
      this.age = 32;
    }

```
#### 4. Исправьте код так, чтобы в `this` попадал нужный контекст. Исправить нужно 3мя способами, как показано в примерах урока.

```js
  const person = {
    name: 'Nikita',
    sayHello: function() {
      setTimeout(function() {
          console.log(this.name + ' says hello to everyone!');
      }, 1000)
    }
  }

  person.sayHello();

  //1 способ
   const person = {
    name: 'Nikita',
    sayHello: function() {
      setTimeout(() => {
          console.log(this.name + ' says hello to everyone!');
      }, 1000)
    }
  }

  //2 способ
  function timeLine() {
          console.log(this.name + ' says hello to everyone!');
      }
 
 const person = {
    name: 'Nikita',
    sayHello() {
      setTimeout(timeLine.bind(this), 1000)
    }
  }

  const person = {
    name: 'Nikita',
    sayHello() {
      setTimeout(timeLine.call(this), 1000)
    }
  }

  //3 ???

```

## Выполните задания

* Установите зависимости `npm install`;
* Допишите функции в `task.js`;
* Проверяйте себя при помощи тестов `npm run test`;
* Создайте Merge Request с решением.
